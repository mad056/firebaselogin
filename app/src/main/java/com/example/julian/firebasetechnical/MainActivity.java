package com.example.julian.firebasetechnical;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class MainActivity extends AppCompatActivity {
    private FirebaseAuth mAuth;
    SharedPreferences settings;
    SharedPreferences.Editor editor;
    static main main = new main();
 boolean check = false;
    EditText zidTextF;
    EditText passwordTextF;
    TextView warningtext;
    boolean found = false;
    CheckBox rememberMeCB;
    Button loginBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mAuth = FirebaseAuth.getInstance();
        loginBtn = findViewById(R.id.login);
        warningtext = findViewById(R.id.Hello);
        zidTextF = findViewById(R.id.zidTextF);
        passwordTextF = findViewById(R.id.passwordTextF);
        rememberMeCB = findViewById(R.id.rememberMeCB);


        try {
            settings = getApplicationContext().getSharedPreferences("saved login", 0);
            editor = settings.edit();
            String username = settings.getString("zid", String.valueOf(0));
            String password = settings.getString("password", String.valueOf(0));
            if (!username.equals("") && !username.equals("0") && !password.equals("") && !password.equals("0")) {
                zidTextF.setText(username);
                passwordTextF.setText(password);
                rememberMeCB.setChecked(true);
                loginBtn.performClick();
            }
        } catch (Exception ex) {
            System.out.println(ex);
        }

    }

    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();
        System.out.println("Update UI Already Signed in");
    }


    public void LoginClicked(View view) {
        mAuth.signInWithEmailAndPassword(main.zidToZmail(zidTextF.getText().toString()), passwordTextF.getText().toString())
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {


                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            main.zid = zidTextF.getText().toString();
                            // Sign in success, update UI with the signed-in user's information
                            System.out.println("signInWithEmail:success");
                            //FirebaseUser user = mAuth.getCurrentUser();
                            if (rememberMeCB.isChecked()) {
                                editor.putString("zid", zidTextF.getText().toString());
                                editor.putString("password", passwordTextF.getText().toString());
                            } else {
                                editor.putString("zid", "");
                                editor.putString("password", "");
                            }
                            System.out.println("Login: True");
                            FirebaseUser currentUser = mAuth.getCurrentUser();
                            System.out.println(currentUser.getUid());
                            CheckUser(currentUser.getUid());
                            editor.apply();
                            //zidTextF.setText("");
                            passwordTextF.setText("");
                            //rememberMeCB.setChecked(false);


                        } else {
                            // If sign in fails, display a message to the user.
                            System.out.println("Login: Failture");
                            //  Log.w(TAG, "createUserWithEmail:failure", task.getException());
                        }
                    }

                });


    }

    public void CheckUser(final String USERID) {
        FirebaseDatabase data = FirebaseDatabase.getInstance();
        DatabaseReference MaxDatabase = data.getReference();
        MaxDatabase.child("Users").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot answerSnapshot : dataSnapshot.getChildren()) {

                    if (answerSnapshot.getKey().equals(USERID)) {
                        System.out.println("User Found");
                        check = true;
                    }

                }
                if (check == false) {
                    System.out.println("User not found");
                    CreateUserDatabase(USERID);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }


    public void CreateUserDatabase(String USERID) {
            FirebaseDatabase database = FirebaseDatabase.getInstance();
            DatabaseReference mdatabaseReference = database.getReference("Users/" + USERID);
            mdatabaseReference.child("Name").setValue("TEST");


    }

    public void  Getinfo() {
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {
            // Name, email address, and profile photo Url
            String name = user.getDisplayName();
            String email = user.getEmail();
            Uri photoUrl = user.getPhotoUrl();

            // Check if user's email is verified
            boolean emailVerified = user.isEmailVerified();

            // The user's ID, unique to the Firebase project. Do NOT use this value to
            // authenticate with your backend server, if you have one. Use
            // FirebaseUser.getToken() instead.
            String uid = user.getUid();
        }
    }
}
